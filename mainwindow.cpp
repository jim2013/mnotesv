#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "previewpage.h"
#include <QtWebChannel>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("mNote Viewer");
    folderModel = new QFileSystemModel();
    folderModel->setRootPath("/home/song/Documents/notes");
    ui->folder->setModel(folderModel);
    ui->folder->setRootIndex(folderModel->index("/home/song/Documents/notes"));
    ui->folder->setColumnHidden(1, true);
    ui->folder->setColumnHidden(2, true);
    ui->folder->setColumnHidden(3, true);

    ui->preview->setContextMenuPolicy(Qt::NoContextMenu);
    PreviewPage *page = new PreviewPage(this);
    ui->preview->setPage(page);
    QWebChannel *channel = new QWebChannel(this);
    channel->registerObject(QStringLiteral("content"), &m_content);
    page->setWebChannel(channel);
    ui->preview->setUrl(QUrl("qrc:/index.html"));

    connect(ui->folder, &QTreeView::doubleClicked, this, &MainWindow::onNoteDC);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onFileOpen()
{

}

void MainWindow::onNoteDC(const QModelIndex &index)
{
    QFileInfo fileInfo = folderModel->fileInfo(index);
    if (fileInfo.isFile()){
        QFile file(fileInfo.absoluteFilePath());
        file.open(QIODevice::ReadOnly);
        m_content.setText(file.readAll());
    }
}

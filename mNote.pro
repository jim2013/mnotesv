#-------------------------------------------------
#
# Project created by QtCreator 2018-11-22T17:20:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets webenginewidgets webchannel

TARGET = mNote
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    previewpage.cpp \
    document.cpp

HEADERS  += mainwindow.h \
    previewpage.h \
    document.h

FORMS    += mainwindow.ui

DISTFILES += \
    resources/3rdparty/marked.js \
    resources/3rdparty/qt_attribution.json \
    resources/3rdparty/markdown.css \
    resources/index.html \
    resources/default.md \
    resources/3rdparty/MARKDOWN-LICENSE.txt \
    resources/3rdparty/MARKED-LICENSE.txt

RESOURCES += \
    resources/mnote.qrc
